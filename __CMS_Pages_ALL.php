<?php


// ################################################################################################################################
echo '<h1>HOMEPAGE</h1>';
// ################################################################################################################################

// ========================= Home page (S)==============================
$html = '<p></p>';

$title = 'Home page';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'home';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Home page (E)==============================

echo '</br>';
// ################################################################################################################################
echo '<h1>VOORWAARDEN</h1>';
// ################################################################################################################################

// ========================= Accountovereenkomst (S)==============================
$html = file_get_contents("_V_Accountovereenkomst.html");

$title = 'Accountovereenkomst';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'accountovereenkomst';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Accountovereenkomst (E)==============================

echo '</br>';
echo '</br>';

// ========================= Algemene voorwaarden (S)==============================
$html = file_get_contents("_V_AlgemeneVoorwaarden.html");

$title = 'Algemene Voorwaarden';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'algemene-voorwaarden';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Algemene voorwaarden (E)==============================

echo '</br>';
echo '</br>';

// ========================= Privacybeleid (S)==============================
$html = file_get_contents("_V_Privacybeleid.html");

$title = 'Privacybeleid';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'privacybeleid';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Privacybeleid (E) ==============================

echo '</br>';
// ################################################################################################################################
echo '<h1>HELPDESK</h1>';
// ################################################################################################################################

// ========================= Annuleren (B) ==============================
$html = file_get_contents("_H_Annuleren.html");

$title = 'Annuleren';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/annuleren';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Annuleren (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Bestellen (B) ==============================
$html = file_get_contents("_H_Bestellen.html");

$title = 'Bestellen';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/bestellen';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Bestellen (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Betalen (B) ==============================
$html = file_get_contents("_H_Betalen.html");

$title = 'Betalen';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/betalen';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Betalen (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Cadeaubonnen (B) ==============================
$html = file_get_contents("_H_Cadeaubonnen.html");

$title = 'Cadeaubonnen';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/cadeaubonnen';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Cadeaubonnen (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Email ons (B) ==============================
$html = file_get_contents("_H_EmailOns.html");

$title = 'Email Ons';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/email-ons';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Email ons (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Garantie (B) ==============================
$html = file_get_contents("_H_Garantie.html");

$title = 'Garantie';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/garantie';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= garantie (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Leveringen (B) ==============================
$html = file_get_contents("_H_Leveringen.html");

$title = 'Leveringen';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/leveringen';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Leveringen (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Mijn account (B) ==============================
$html = file_get_contents("_H_MijnAccount.html");

$title = 'Mijn Account';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/mijn-account';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Mijn account (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Overzicht (B) ==============================
$html = file_get_contents("_H_Overzicht.html");

$title = 'Helpdesk';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/overzicht';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Overzicht (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Retourneren (B) ==============================
$html = file_get_contents("_H_Retourneren.html");

$title = 'Retourneren';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'helpdesk/retourneren';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Retourneren (E) ==============================


echo '</br>';
// ################################################################################################################################
echo '<h1>OVER ONS</h1>';
// ################################################################################################################################

// ========================= Carrières (B) ==============================
$html = file_get_contents("_O_Carrieres.html");

$title = 'Carrières';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'over-ons/carrieres';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Carrières (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Contacteer ons (B) ==============================
$html = file_get_contents("_O_ContacteerOns.html");
$title = 'Contacteer Ons';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'over-ons/contacteer-ons';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Contacteer ons (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Leiderschap (B) ==============================
$html = file_get_contents("_O_Leiderschap.html");

$title = 'Leiderschap';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'over-ons/leiderschap';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Leiderschap (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Onze Voordelen (B) ==============================
$html = file_get_contents("_O_OnzeVoordelen.html");

$title = 'Onze Voordelen';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'over-ons/onze-voordelen';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Onze Voordelen (E) ==============================

echo '</br>';
echo '</br>';

// ========================= Overzicht (B) ==============================
$html = file_get_contents("_O_Overzicht.html");

$title = 'Over Ons';
$template = 'one_column';
$metak = 'meta,keywords';
$metad = 'meta description';
$id = 'over-ons/overzicht';
$chead =  '';
$stores = array(0); //available for all store views


include_once 'app/Mage.php';
umask(0);
Mage::app("default");
error_reporting(E_ALL);


//If cms/page already exists (or is active), delete it:
$state = Mage::getModel('cms/page')->load($id,'identifier')->getIsActive();
if ($state == 1){
	Mage::getModel('cms/page')->load($id,'identifier')->delete();
	echo 'old page "'.$id.'" deleted<br/>';
} else {
	echo 'old page "'.$id.'" not found<br/>';
}

//Create array with the new cms/page data
$cmsPageData = array(
    'title' 					=> $title,
    'root_template' 			=> $template,
    'meta_keywords' 			=> $metak,
    'meta_description'		    => $metad,
    'identifier' 				=> $id,
    'content_heading' 			=> $chead,
    'stores' 					=> $stores,
	'content' 					=> $html
);

//Create the cms/page
Mage::getModel('cms/page')->setData($cmsPageData)->save();
echo 'new page "'.$id.'" created';
// ========================= Overzicht (E) ==============================