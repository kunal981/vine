<?php

$html = 'something';

include_once 'app/Mage.php';
umask(0);
Mage::app("default");

error_reporting(E_ALL);


//Create Static Block
$cmsPageData = array(
    'title' => 'Pagina Titel',
    'root_template' => 'one_column',
    'meta_keywords' => 'meta,keywords',
    'meta_description' => 'meta description',
    'identifier' => 'pagina-url',
    'content_heading' => 'Content Header',
    'stores' => array(0),//available for all store views
    'content' => $html
);

Mage::getModel('cms/page')->setData($cmsPageData)->save();