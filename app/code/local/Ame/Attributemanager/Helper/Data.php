<?php

class Ame_Attributemanager_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected $_categories;

    protected $_categoriesNotInStore;

    protected $_attributes;

    protected $_excludedAttributes = array('url_key');


    public function getCategories()
    {
        if (is_null($this->_categories)) {
            $categoriesIds = $this->getCategoryIds();

            if(!is_array($categoriesIds)) {
                $categoriesIds = array(0);
            }

            $this->_categories = Mage::getResourceModel('catalog/category_collection')
                ->setStoreId($this->getSelectedStoreId())
                ->addIdFilter($categoriesIds);
        }

        return $this->_categories;
    }

    public function getCategoryIds()
    {
        $session = Mage::getSingleton('adminhtml/session');

        if ($this->_getRequest()->isPost() && $this->_getRequest()->getActionName()=='edit') {
            $session->setCategoryIds($this->_getRequest()->getParam('category', null));
        }

        return $session->getCategoryIds();
    }

    public function getSelectedStoreId()
    {
        return (int) $this->_getRequest()->getParam('store', 0);
    }

    public function getCategoriesSetIds()
    {
        return $this->getCategories()->getSetIds();
    }

    public function getAttributes()
    {
        if (is_null($this->_attributes)) {
            $this->_attributes = $this->getCategories()->getEntity()->getEntityType()->getAttributeCollection()
                ->addIsNotUniqueFilter()
                ->setInAllAttributeSetsFilter($this->getCategoriesSetIds());

            foreach ($this->_excludedAttributes as $attributeCode) {
                $this->_attributes->addFieldToFilter('attribute_code', array('neq'=>$attributeCode));
            }

            $this->_attributes->load();
            foreach($this->_attributes as $attribute) {
                $attribute->setEntity($this->getCategories()->getEntity());
            }
        }

        return $this->_attributes;
    }

    public function getCategoriesNotInStoreIds()
    {
        if (is_null($this->_categoriesNotInStore)) {
            $this->_categoriesNotInStoreIds = array();
        }

        return $this->_categoriesNotInStoreIds;
    }
}

?>