<?php
require_once 'lib/simple_html_dom.php';

class Ame_Ordertracking_IndexController extends Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();
		
        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }
	
	public function dpdAction()
    {
		date_default_timezone_set('Europe/Berlin');
		$this->loadLayout();
		$this->renderLayout();
    }  
	
}
