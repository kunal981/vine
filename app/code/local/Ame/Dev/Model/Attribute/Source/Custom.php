<?php
class Ame_Dev_Model_Attribute_Source_Custom extends Mage_Eav_Model_Entity_Attribute_Source_Abstract{
    
    public function getAllOptions(){
      
        $typeproduct_attribute = 'type_product';                  
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product',$typeproduct_attribute);
        if ($attribute->usesSource()) {
            $typeproduct_options = $attribute->getSource()->getAllOptions(false);
        }
        foreach($typeproduct_options as $optiondata)
        {
            $typeproductcode[]=array('value'=>$optiondata['value'],'label' =>$optiondata['label']);
        }       
         
        return $typeproductcode;    
    }
} ?>