<?php
$installer = $this;
$installer->startSetup();
$attribute  = array(
        'group'             => 'General',
        'type'              => 'varchar',
        'backend'           => '',
        'frontend_input'    => '',
        'frontend'          => '',
        'label'             => 'Product Attribute',
        'input'             => 'multiselect',
        'class'             => '',
        'source'            => 'dev/attribute_source_custom',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'backend'           => 'eav/entity_attribute_backend_array',
	'visible'           => 1,
	'required'          => 0,
	'user_defined'      => 1,
	'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
);
$installer->addAttribute('catalog_category', 'attributecolor', $attribute);
$installer->endSetup();
?>
