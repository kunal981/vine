<?php
class Ame_Merk_Block_Adminhtml_Baz extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        // The blockGroup must match the first half of how we call the block, and controller matches the second half
        // ie. foo_bar/adminhtml_baz
        $this->_blockGroup = 'ame_merk';
        $this->_controller = 'adminhtml_baz';
        $this->_headerText = $this->__('Baz');
         
        parent::__construct();
    }
}