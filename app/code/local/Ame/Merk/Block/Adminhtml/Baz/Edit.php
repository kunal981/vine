<?php
class Ame_Merk_Block_Adminhtml_Baz_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {  
        $this->_blockGroup = 'ame_merk';
        $this->_controller = 'adminhtml_baz';
     
        parent::__construct();
     
        $this->_updateButton('save', 'label', $this->__('Save Brand'));
        $this->_updateButton('delete', 'label', $this->__('Delete Brand'));
    }  

    public function getHeaderText()
    {  
        if (Mage::registry('ame_merk')->getId()) {
            return $this->__('Edit Brand');
        }  
        else {
            return $this->__('New Brand');
        }  
    }  
}