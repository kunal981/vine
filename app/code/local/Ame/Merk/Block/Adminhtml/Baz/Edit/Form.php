<?php
class Ame_Merk_Block_Adminhtml_Baz_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
     
        $this->setId('ame_merk_baz_form');
        $this->setTitle($this->__('Brand Information'));
    }  
     
    protected function _prepareForm()
    {       
        $categories = Mage::getModel('catalog/category')->getCollection()
        ->addAttributeToSelect('id')
        ->addAttributeToSelect('name')
        ->addAttributeToSelect('custom_attribute')
        ->addAttributeToSelect('url_key')
        ->addAttributeToSelect('url')
        ->addAttributeToSelect('is_active');

        foreach ($categories as $category)
        {
            if ($category->getIsActive()) { 
                $entity_id = $category->getId();
                $custom[] = $category->getCustomAttribute();
                $name = $category->getName();
                $url_key = $category->getUrlKey();
                $url_path = $category->getUrl();
                $myvalues[$name] = $name;
            }
        }

      $res[]=array();
      $i=0;
      foreach($custom as $keyCustom=>$valueCustom){
          
          $res[$i]['value']=$valueCustom;
          $res[$i]['label']=$valueCustom;
          $i++;
      }
$attribute_code='merken';	
$option_arr = array();
$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'merken');

        $model = Mage::registry('ame_merk');
     
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method'    => 'post'
        ));
     
        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => Mage::helper('checkout')->__('Brand Information'),
            'class'     => 'fieldset-wide',
        ));
     
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }
        $fieldset->addField('category', 'select', array(
            'name'      => 'category',
            'label'     => Mage::helper('checkout')->__('Category'),
            'title'     => Mage::helper('checkout')->__('Category'),
            'required'  => false,
            'onclick' => "",
            'onchange' => "",
            'values' => $myvalues,
        ));

       
        foreach ($attribute->getSource()->getAllOptions(false) as $option) {
            $option_arr[] = array("value" => $option['value'], "label" => $option['label']);
        } 
        $fieldset->addField('brands', 'multiselect', array(
          'label'     => Mage::helper('checkout')->__('Brands'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'brands',
          'onclick' => "return false;",
          'onchange' => "return false;",
         
          'values' => $option_arr,
          'disabled' => false,
          'readonly' => false,
          'tabindex' => 1
        ));

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
     
        return parent::_prepareForm();
    }  
}