<?php
class Ame_Merk_Adminhtml_BazController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {  
        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction()
            ->renderLayout();
    }  
     
    public function newAction()
    {  
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
    }  
     
    public function editAction()
    {
        $this->_initAction();
     
        // Get id if available
        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('ame_merk/baz');
     
        if ($id) {
            // Load record
            $model->load($id);
     
            // Check if record is loaded
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This Brand no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }  
        }  
     
        $this->_title($model->getId() ? $model->getName() : $this->__('New Brand'));
     
        $data = Mage::getSingleton('adminhtml/session')->getBazData(true);
        if (!empty($data)) {
            $model->setData($data);
        }  
     
        Mage::register('ame_merk', $model);
     
        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit Brand') : $this->__('New Brand'), $id ? $this->__('Edit Brand') : $this->__('New Brand'))
            ->_addContent($this->getLayout()->createBlock('ame_merk/adminhtml_baz_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }
     
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
           $model = Mage::getSingleton('ame_merk/baz');
           $model->setData($postData);         
           $fbrand=implode(',', $postData['brands']);         
           $model->setBrands($fbrand);
 
            try {
                $model->save();
 
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Brand has been saved.'));
                $this->_redirect('*/*/');
                return;
            }  
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this Brand.'));
            }
 
            Mage::getSingleton('adminhtml/session')->setBazData($postData);
            $this->_redirectReferer();
        }
    }
    
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $diog_collection = Mage::getModel('ame_merk/baz')
                ->load($id);

            try {
                $diog_collection->delete();
                $this->_getSession()->addSuccess($this->__('The Access code has been deleted.'));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
       $this->_redirect('*/*/');
    }
     
    public function messageAction()
    {
        $data = Mage::getModel('ame_merk/baz')->load($this->getRequest()->getParam('id'));
        echo $data->getContent();
    }
     
    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('ame_merk_baz/ame_merk_baz')
            ->_title($this->__('AME Menu'))->_title($this->__('Brand Navigation'))
            ->_addBreadcrumb($this->__('AME Menu'), $this->__('AME Menu'))
            ->_addBreadcrumb($this->__('Brand Navigation'), $this->__('Brand Navigation'));
        return $this;
    }
     
    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ame_merk_baz/ame_merk_baz');
    }
    
}