<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
 
/**
 * Create table 'ame_merk_baz'
 */
$table = $installer->getConnection()
    // The following call to getTable('ame_merk/baz') will lookup the resource for ame_merk (ame_merk_mysql4), and look
    // for a corresponding entity called baz. The table name in the XML is ame_merk_baz, so ths is what is created.
    ->newTable($installer->getTable('ame_merk/baz'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'ID')
    ->addColumn('category', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
        'nullable'  => false,
        ), 'Category')
    ->addColumn('brands', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
        'nullable'  => false,
        ), 'Brands')
    ->addColumn('extra', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
        'nullable'  => false,
     ), 'Extra');
$installer->getConnection()->createTable($table);
 
$installer->endSetup();