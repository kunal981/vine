<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class Ame_Sugarcrm_CartController extends Mage_Checkout_CartController
{
   
    
    /**
     * Delete shoping cart item action
     */
    public function ajaxdeleteAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)->save();
                $count= Mage::helper('checkout/cart')->getCart()->getItemsCount();
                                
                $result['success']='success';
                $result['id']=$id;
                $result['subtotal']=number_format(Mage::getModel('checkout/cart')->getQuote()->getSubtotal(),2);
                $result['grandtotal']=number_format(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal(),2);
                $result['count']=$count;
              
                echo json_encode($result);
                exit;
                
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Cannot remove the item.'));
                Mage::logException($e);
            }
         
      }
      
        $this->_redirectReferer(Mage::getUrl('*/*'));
    }
        function couponPostAction() 
        {
          // if not ajax have parent deal with result 
          $old_gt=number_format(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal(),2);
          if(!isset($_POST['ajax'])) 
          { 
            parent::couponPostAction(); 
            return; 
          } 

          $msg = ''; 

          $couponCode = (string) $this->getRequest()->getParam('coupon_code'); 
              if ($this->getRequest()->getParam('remove') == 1) { 
                  $couponCode = ''; 
              } 
              $oldCouponCode = $this->_getQuote()->getCouponCode(); 

              try { 
                  $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true); 
                  $this->_getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '') 
                      ->collectTotals() 
                      ->save(); 
                  if (!strlen($couponCode) && !strlen($oldCouponCode)) { 
                  $msg=$this->__('blank', Mage::helper('core')->htmlEscape($couponCode));
                  } 

                  if ($couponCode) {
                      if ($couponCode == $this->_getQuote()->getCouponCode()) {
                          $msg=$this->__('valid', Mage::helper('core')->htmlEscape($couponCode));
                      } 
                      else { 
                         $msg = $this->__('invalid', Mage::helper('core')->htmlEscape($couponCode)); 
                         
                      } 
                  } else { 
                     $msg= $this->_getSession()->addSuccess($this->__('Coupon code was canceled.')); 
                  } 

              } catch (Mage_Core_Exception $e) { 
                  $msg = $e->getMessage(); 
              } catch (Exception $e) { 
                   $msg = $this->__('Cannot apply the coupon code.'); 
                  Mage::logException($e); 
              }
              $simbol= Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
              $gt = number_format(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal(),2);
              $result['msg']=$msg;
              $result['gt']=$simbol.$gt;
              $result['oldgt']=$old_gt;
              $result['gtv']=$gt;
              echo json_encode($result);
              exit;
        }
        function couponPost2Action() 
        { 
          // if not ajax have parent deal with result 

          if(!isset($_POST['ajax'])) 
          { 
            parent::couponPostAction(); 
            return; 
          } 

          $msg = ''; 
          $old_gt=number_format(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal(),2);
          $couponCode = (string) $this->getRequest()->getParam('coupon_code_payment'); 
              if ($this->getRequest()->getParam('remove') == 1) { 
                  $couponCode = ''; 
              } 
              $oldCouponCode = $this->_getQuote()->getCouponCode(); 

              try { 
                  $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true); 
                  $this->_getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '') 
                      ->collectTotals() 
                      ->save(); 
                  if (!strlen($couponCode) && !strlen($oldCouponCode)) { 
                  $msg=$this->__('blank', Mage::helper('core')->htmlEscape($couponCode));
                  } 

                  if ($couponCode) { 
                      if ($couponCode == $this->_getQuote()->getCouponCode()) {
//                          $msg=$this->_getSession()->addSuccess( 
//                              $this->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponCode)) 
//                          ); 
                          $msg=$this->__('valid', Mage::helper('core')->htmlEscape($couponCode));
                      } 
                      else { 
                         $msg = $this->__('invalid', Mage::helper('core')->htmlEscape($couponCode)); 
                         
                      } 
                  } else { 
                     $msg= $this->_getSession()->addSuccess($this->__('Coupon code was canceled.')); 
                  } 

              } catch (Mage_Core_Exception $e) { 
                  $msg = $e->getMessage(); 
              } catch (Exception $e) { 
                   $msg = $this->__('Cannot apply the coupon code.'); 
                  Mage::logException($e); 
              } 
              $simbol= Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
              $gt = number_format(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal(),2);
              $result['msg']=$msg;
              $result['gt']=$simbol.$gt;
              $result['oldgt']=$old_gt;
              $result['gtv']=$gt;
              echo json_encode($result);
              exit;
        }
        
        public function addAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                //$this->_goBack();
                $this->_redirectReferer();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
                $this->_redirectReferer();
            } else {
                //$this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
                $this->_redirectReferer();
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            //$this->_goBack();
            $this->_redirectReferer();
        }
    }
    
    public function quickAddAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                //$this->_goBack();
                $this->_redirectReferer();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
                $this->_redirectReferer();
            } else {
                $this->_redirectReferer();
                //$this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            //$this->_goBack();
            $this->_redirectReferer();
        }
    }
}