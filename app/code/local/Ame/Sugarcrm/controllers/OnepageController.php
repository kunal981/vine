<?php
require_once 'Mage/Checkout/controllers/OnepageController.php';
class Ame_Sugarcrm_OnepageController extends Mage_Checkout_OnepageController
{
    protected function _expireAjax()
    {
       
        if (!$this->getOnepage()->getQuote()->hasItems()
            || $this->getOnepage()->getQuote()->getHasError()
            || $this->getOnepage()->getQuote()->getIsMultiShipping()
        ) {
            $this->_ajaxRedirectResponse();
            return true;
        }
        $action = $this->getRequest()->getActionName();
        return false;
    }
     public function indexAction()
    {
         if(!Mage::getSingleton('customer/session')->isLoggedIn())
         {
             $this->_redirect('customer/account/login');
         }
        if (!Mage::helper('checkout')->canOnepageCheckout()) {
            Mage::getSingleton('checkout/session')->addError($this->__('The onepage checkout is disabled.'));
            $this->_redirect('checkout/cart');
            return;
        }
        $quote = $this->getOnepage()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message') ?
                Mage::getStoreConfig('sales/minimum_order/error_message') :
                Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');

            Mage::getSingleton('checkout/session')->addError($error);
            $this->_redirect('checkout/cart');
            return;
        }
        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure' => true)));
        $this->getOnepage()->initCheckout();
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Checkout'));
        $this->renderLayout();
    }
    public function saveShippingAction()
    {
        /*Customer data from database=====================================================================*/
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customer_data=Mage::getModel('customer/address')->load($customer->getDefaultShipping())->getData();
        $first_name=trim($customer_data['firstname']);
        $last_name=trim($customer_data['lastname']);
        $street=trim($customer_data['street']);
        $city=trim($customer_data['city']);
        $region_id=trim($customer_data['region_id']);
        $postcode=trim($customer_data['postcode']);
        $country_id=trim($customer_data['country_id']);
        $telephone=trim($customer_data['telephone']);
        /*Customer Data from Form Post=====================================================================*/
        $st=$_POST['shipping']['street'];
        $st_data=trim(implode(" ",$st));
        $post_first_name=trim($_POST['shipping']['firstname']);
        $post_last_name=trim($_POST['shipping']['lastname']);
        $post_city=trim($_POST['shipping']['city']);
        $post_region_id=trim($_POST['shipping']['region_id']);
        $post_postcode=trim($_POST['shipping']['postcode']);
        $post_country_id=trim($_POST['shipping']['country_id']);
        $post_telephone=trim($_POST['shipping']['telephone']);
            if(
                $first_name!==$post_first_name 
                || $last_name!==$post_last_name 
                || $street!==$st_data 
                || $city!==$post_city
                || ($region_id!==$post_region_id && $region_id!=0)
                || $postcode!==$post_postcode
                || $country_id!==$post_country_id
                || $telephone!==$post_telephone
               )
            {   
                if(isset($_POST['shipping_save_in_address_book']))
         {
                        $address   = Mage::getModel('customer/address');
                        $address->setCustomerId($customer->getId())
                        ->setFirstname($post_first_name)
                        ->setMiddleName($customer->getMiddlename())
                        ->setLastname($post_last_name)
                        ->setCountryId($post_country_id)
                        ->setRegionId($post_region_id)
                        ->setPostcode($post_postcode)
                        ->setCity($post_city)
                        ->setTelephone($post_telephone)
                        ->setStreet($st_data)
                        ->setIsDefaultBilling('1')
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');
         }
         else
         {
          $address   = Mage::getModel('customer/address');
                        $address->setCustomerId($customer->getId())
                        ->setFirstname($post_first_name)
                        ->setMiddleName($customer->getMiddlename())
                        ->setLastname($post_last_name)
                        ->setCountryId($post_country_id)
                        ->setRegionId($post_region_id)
                        ->setPostcode($post_postcode)
                        ->setCity($post_city)
                        ->setTelephone($post_telephone)
                        ->setStreet($st_data)
                        ->setSaveInAddressBook('1');   
         }
                try {
                        $address->save();
                    }
                catch (Exception $e) {
                    Zend_Debug::dump($e->getMessage());
                }
            }
            if ($this->_expireAjax()) {
                return;
            }
            if ($this->getRequest()->isPost()) {
                $data = $this->getRequest()->getPost('shipping', array());
                $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
                $result = $this->getOnepage()->saveShipping($data, $customerAddressId);

                if (!isset($result['error'])) {
                    $result['goto_section'] = 'shipping_method';
                    $result['update_section'] = array(
                        'name' => 'shipping-method',
                        'html' => $this->_getShippingMethodsHtml()
                    );
                }
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            }
        }
}