<?php
$m = intval(self::$options->getValue('selectors-margin'));
$wm = intval(self::$options->getValue('thumb-max-width'));
?>

<!-- Begin magiczoomplus -->
<div class="AmeContainer" style="max-width: <?php echo $wm?>px;">
    <?php if($main) echo $main; ?>

    <?php if(isset($message)):?>
        <div class="AmeMessage"><?php echo $message?></div>
    <?php endif?>

    <?php if(count($thumbs)):?>
    <div id="AmeSelectors<?php echo $pid?>" class="more-views AmeSelectorsContainer" style="margin-top: <?php echo $m;?>px">
        <h4><?php echo $moreViewsCaption ?></h4>
        <ul>
        <?php foreach($thumbs as $thumb):?>
            <li><?php echo $thumb?></li>
        <?php endforeach?>
        </ul>
    </div>
    <?php endif?>
</div>
<!-- End  -->
