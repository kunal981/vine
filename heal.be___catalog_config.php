<?php

require_once 'app' . DIRECTORY_SEPARATOR . 'Mage.php';
Mage::app();

/* ============================================================================================ */
echo '__________________________________________________<br/>';
echo 'GENERAL<br/>';
/** ================================
	Configuration -> General -> ...
================================ **/

/*Here we assigned which attribute we are going to fetch*/
$colorcode_attribute = 'attributecolor';  

/*Fetch all the data of that attribute*/
$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_category',$colorcode_attribute);
if ($attribute->usesSource()) {
    $colorcode_options = $attribute->getSource()->getAllOptions(false);
}

/* Baby & Zwangerschap */
$Accessoires    = array();
$Babykwaaltjes  = array();
$Huidverzorging = array();
$Babymedicatie  = array();
$MelkVoeding    = array();
$Vitaminen      = array();
$Zwangerschap   = array();

/* Eerste Hulp & Thuiszorg */
$EHBOVerband      = array();
$Thuiszorg        = array(); 
$MetenWegenTesten = array();

/* Persoonlijke Verzorging */
$Mondverzorging  = array();
$Deodorants      = array();
$BadDouche       = array();
$Zonneproducten = array();
$Haarverzorging  = array();
$HandenVoeten     = array();

/* Geneesmiddelen */


/* Huidverzorging & Make-Up */
$DagNachtcreme     = array();
$OlieCremeMelk     = array();
//
//
$Gezichtsreiniging = array();	
$Lipverzorging     = array();
$MakeUpParfum      = array();
$MaskersPeelings   = array();
$Oogcontouren      = array();
//

/* Welzijn & Gezondheid */
$Pijnstilling 				= array();
$OgenOren 					= array();
$MaagDarmen 				= array();
$KeelAdemhaling 			= array();
$NachtrustOntspanningStress = array();
$VermoeideBenenSpieren 		= array();

/* Fit & Slank */
$AfslankendeCremes 		  = array();
$AntiCellulitis 		  = array();
$AfslankendeThee 		  = array();
$Drainage 				  = array();
$VetbindersVetverbranders = array();
$Eetlustremmers 		  = array();
$Sportvoeding 			  = array();

/* Natuurlijk Gezond */	
$Theeen 	  = array();
$BadMassage   = array();
$Fytotherapie = array();
$Homeopathie  = array();

/* Vitaminen & Supplementen */
$SpecifiekeSupp    = array();
$Omega3Vetzuren    = array();
$CalciumMineralen  = array();
$VitamineAK        = array();
$StuderenGeheugen  = array();
$Multivitaminen    = array();
$NatuurlijkAfweer  = array();
$OpkikkersEnergie  = array();
$ProbioticaSpijs   = array();
$Q10EnzymeAmino    = array();
$Voedingscosmetica = array();


/*Assign attribute values to variables having same name as of attribute label*/
foreach($colorcode_options as $optiondata)
{

	/* =================== */
	/* Baby & Zwangerschap */
	/* =================== */	
	
	/* Accessoires */
	switch ($optiondata['label']) {
 		case 'Thermometers':
			$Accessoires[] = $optiondata['value'];
			break;
	}	
	/* Babykwaaltjes */
	switch ($optiondata['label']) {
 		case 'Babytandjes':
			$Babykwaaltjes[] = $optiondata['value'];
			break;
		case 'Melkkorstjes':
			$Babykwaaltjes[] = $optiondata['value'];
			break;
 		case 'Neus Vrij Maken':
			$Babykwaaltjes[] = $optiondata['value'];
			break;
	}	
	/* Huidverzorging */
	switch ($optiondata['label']) {
 		case 'Hydratatie':
			$Huidverzorging[] = $optiondata['value'];
			break;
		case 'Kwaaltjes':
			$Huidverzorging[] = $optiondata['value'];
			break;
 		case 'Luiercrème':
			$Huidverzorging[] = $optiondata['value'];
			break;
 		case 'Melkkorstjes':
			$Huidverzorging[] = $optiondata['value'];
			break;
	}	
	/* Babymedicatie */
	switch ($optiondata['label']) {
 		case 'Geneesmiddelen':
			$Babymedicatie[] = $optiondata['value'];
			break;
		case 'Wondgenezing':
			$Babymedicatie[] = $optiondata['value'];
			break;
	}
	/* Melk & Voeding */
	switch ($optiondata['label']) {
 		case '___________________________________________________________________':
			$MelkVoeding[] = $optiondata['value'];
			break;
	}
	/* Vitaminen */
	switch ($optiondata['label']) {
 		case 'Vitamine C':
			$Vitaminen[] = $optiondata['value'];
			break;
 		case 'Vitamine D':
			$Vitaminen[] = $optiondata['value'];
			break;
 		case 'Vitaminen':
			$Vitaminen[] = $optiondata['value'];
			break;
	}
	/* Zwangerschap */
	switch ($optiondata['label']) {
 		case 'Borstvoeding':
			$Zwangerschap[] = $optiondata['value'];
			break;
 		case 'Lichaamsverzorging':
			$Zwangerschap[] = $optiondata['value'];
			break;
 		case 'Vitaminen':
			$Zwangerschap[] = $optiondata['value'];
			break;
 		case 'Zwangerschaptest':
			$Zwangerschap[] = $optiondata['value'];
			break;
	}
	
	/* ======================= */
	/* Eerste Hulp & Thuiszorg */
	/* ======================= */	
	
	/* EHBO & Verbandmateriaal */
	switch ($optiondata['label']) {
 		case 'Pleisters & Verbanden':
			$EHBOVerband[] = $optiondata['value'];
			break;
	}	
	/* Thuiszorg */
	switch ($optiondata['label']) {
 		case 'Welzijn & Comfort':
			$Thuiszorg[] = $optiondata['value'];
			break;
	}
	/* Meten, Wegen & Testen */
	switch ($optiondata['label']) {
 		case 'Thermometers':
			$MetenWegenTesten[] = $optiondata['value'];
			break;
	}		
	
	/* ======================= */
	/* Persoonlijke Verzorging */
	/* ======================= */	
	
	/* Mondverzorging */
	switch ($optiondata['label']) {
 		case 'Babytandjes':
			$Mondverzorging[] = $optiondata['value'];
			break;
		case 'Tandpasta':
			$Mondverzorging[] = $optiondata['value'];
			break;
 		case 'Mondspoeling & Mondwater':
			$Mondverzorging[] = $optiondata['value'];
			break;
	}	
	/* Deodorants */
	switch ($optiondata['label']) {
 		case 'Deodorants':
			$Deodorants[] = $optiondata['value'];
			break;
		case 'Deo Spray':
			$Deodorants[] = $optiondata['value'];
			break;
	}
	/* Bad & Douche */
	switch ($optiondata['label']) {
 		case 'Schuimgel':
			$BadDouche[] = $optiondata['value'];
			break;
		case 'Doucheolie':
			$BadDouche[] = $optiondata['value'];
			break;
 		case 'Douchegel':
			$BadDouche[] = $optiondata['value'];
			break;
		case 'Waslotion':
			$BadDouche[] = $optiondata['value'];
			break;
 		case 'Badolie':
			$BadDouche[] = $optiondata['value'];
			break;
		case 'Body Scrub':
			$BadDouche[] = $optiondata['value'];
			break;
	}
	/* Zonneproducten */
	switch ($optiondata['label']) {
 		case 'Zonnebrand':
			$Zonneproducten[] = $optiondata['value'];
			break;
		case 'Zelfbruiner':
			$Zonneproducten[] = $optiondata['value'];
			break;
	}
	/* Haarverzorging */
	switch ($optiondata['label']) {
 		case 'Shampoo':
			$Haarverzorging[] = $optiondata['value'];
			break;
	}
	/* Handen & Voeten */
	switch ($optiondata['label']) {
 		case 'Handcrème':
			$HandenVoeten[] = $optiondata['value'];
			break;
		case 'Handzeep':
			$HandenVoeten[] = $optiondata['value'];
			break;
		case 'Manicure & Pedicure':
			$HandenVoeten[] = $optiondata['value'];
			break;
	}

	/* ============== */
	/* Geneesmiddelen */
	/* ============== */


	/* ======================== */
	/* Huidverzorging & Make-Up */
	/* ======================== */		
		
	/* Dag & Nachtcremes */
	switch ($optiondata['label']) {
	case 'Dagcrème':
		$DagNachtcreme[] = $optiondata['value'];
		break;
	case 'Emulsie':
		$DagNachtcreme[] = $optiondata['value'];
		break;
	case 'Nachtcrème':
		$DagNachtcreme[] = $optiondata['value'];
		break;
	case 'Serum':
		$DagNachtcreme[] = $optiondata['value'];
		break;
	}
	/* Olie, Creme & Melk */
	switch ($optiondata['label']) {
	case 'Balsem':
		$OlieCremeMelk[] = $optiondata['value'];
		break;
	case 'Bodylotion':
		$OlieCremeMelk[] = $optiondata['value'];
		break;
	case 'Bodymilk':
		$OlieCremeMelk[] = $optiondata['value'];
		break;
	case 'Lichaamsolie':
		$OlieCremeMelk[] = $optiondata['value'];
		break;
	}
	/* Deodorants */ // zie Persoonlijke Verzorging -> Deodorants

	/* Bad & Douche */ // zie Persoonlijke Verzorging -> Bad & Doucheµ

	/* Gezichtsreiniging */
	switch ($optiondata['label']) {
	case 'Micellaire Reiniging':
		$Gezichtsreiniging[] = $optiondata['value'];
		break;
	case 'Oogontschminking':
		$Gezichtsreiniging[] = $optiondata['value'];
		break;
	case 'Reinigingsmelk':
		$Gezichtsreiniging[] = $optiondata['value'];
		break;
	case 'Tonics & Lotions':
		$Gezichtsreiniging[] = $optiondata['value'];
		break;
	}
	/* Lipverzorging */
	switch ($optiondata['label']) {
	case 'Lipbalsem':
		$Lipverzorging[] = $optiondata['value'];
		break;
	case 'Lipstick':
		$Lipverzorging[] = $optiondata['value'];
		break;
	}
	/* Make-Up & Parfums */
	switch ($optiondata['label']) {
	case 'Foundation':
		$MakeUpParfum[] = $optiondata['value'];
		break;
	}
	/* Maskers & Peelings */
	switch ($optiondata['label']) {
	case 'Masker':
		$MaskersPeelings[] = $optiondata['value'];
		break;
	case 'Peeling':
		$MaskersPeelings[] = $optiondata['value'];
		break;
	}
	/* Oogcontouren */
	switch ($optiondata['label']) {
	case 'Oogcontourcrème':
		$Oogcontouren[] = $optiondata['value'];
		break;
	case 'Oogcontourgel':
		$Oogcontouren[] = $optiondata['value'];
		break;
	case 'Oogcrème':
		$Oogcontouren[] = $optiondata['value'];
		break;
	}
	/* Zonneproducten */ // zie Persoonlijke Verzorging -> Zonneproducten

	
	/* ==================== */
	/* Welzijn & Gezondheid */
	/* ==================== */		
			
	/* Pijnstilling */
	switch ($optiondata['label']) {
	case 'Welzijn & Comfort':
		$Pijnstilling[] = $optiondata['value'];
		break;
	}	
	/* Ogen & Oren */	
	switch ($optiondata['label']) {
	case 'Kwaaltjes':
		$OgenOren[] = $optiondata['value'];
		break;
	case 'Oordopjes':
		$OgenOren[] = $optiondata['value'];
		break;
	case 'Snurken':
		$OgenOren[] = $optiondata['value'];
		break;
	case 'Goed voor het Gehoor':
		$OgenOren[] = $optiondata['value'];
		break;
	case 'Oogverzorging':
		$OgenOren[] = $optiondata['value'];
		break;
	case 'Oorreiniging':
		$OgenOren[] = $optiondata['value'];
		break;
	}	
	/* Maag & Darmen */
	switch ($optiondata['label']) {
	case 'Voor een Goede Darmfunctie':
		$MaagDarmen[] = $optiondata['value'];
		break;
	case 'Bevordert de Spijsvertering':
		$MaagDarmen[] = $optiondata['value'];
		break;
	}		
	/* Keel & Ademhaling */
	switch ($optiondata['label']) {
	case 'Brengt de Keel tot Rust':
		$KeelAdemhaling[] = $optiondata['value'];
		break;
	case 'Gemakkelijk Ademhalen':
		$KeelAdemhaling[] = $optiondata['value'];
		break;
	}		
	/* Nachtrust, Ontspanning & Stress */	
	switch ($optiondata['label']) {
	case 'Oordopjes':
		$NachtrustOntspanningStress[] = $optiondata['value'];
		break;
	case 'Nachtrust & Ontspanning':
		$NachtrustOntspanningStress[] = $optiondata['value'];
		break;
	case 'Rust & Ontspanning':
		$NachtrustOntspanningStress[] = $optiondata['value'];
		break;
	case 'Snurken':
		$NachtrustOntspanningStress[] = $optiondata['value'];
		break;
	}	
	/* Vermoeide Benen & Spieren */	
	switch ($optiondata['label']) {
	case 'Vermoeide Benen':
		$VermoeideBenenSpieren[] = $optiondata['value'];
		break;
	case 'Vermoeide Spieren':
		$VermoeideBenenSpieren[] = $optiondata['value'];
		break;
	}	
		
	/* =========== */
	/* Fit & Slank */
	/* =========== */		
		
	/* Afslankende Cremes */
	switch ($optiondata['label']) {
		case '___________________________________________________________________':
			$AfslankendeCremes[] = $optiondata['value'];
			break;
	}
	/* Anti-Cellulitis */
	switch ($optiondata['label']) {
		case 'Vetverbranders':
			$AntiCellulitis[] = $optiondata['value'];
			break;
	}
	/* Afslankende Thee */
	switch ($optiondata['label']) {
		case 'Kruidenthee':
			$AfslankendeThee[] = $optiondata['value'];
			break;
	}
	/* Drainage */
	switch ($optiondata['label']) {
		case 'Kruidenthee':
			$Drainage[] = $optiondata['value'];
			break;
		case 'Vetverbranders':
			$Drainage[] = $optiondata['value'];
			break;
	}	
	/* Vetbinders & Vetverbranders */
	switch ($optiondata['label']) {
		case 'Vetverbranders':
			$VetbindersVetverbranders[] = $optiondata['value'];
			break;
		case 'Vetbinders':
			$VetbindersVetverbranders[] = $optiondata['value'];
			break;
	}
	/* Eetlustremmers */
	switch ($optiondata['label']) {
		case 'Eetlustremmers':
			$Eetlustremmers[] = $optiondata['value'];
			break;
	}
	/* Sportvoeding */
	switch ($optiondata['label']) {
		case 'Recuperatie':
			$Sportvoeding[] = $optiondata['value'];
			break;
	}
	
	/* ================= */
	/* Natuurlijk Gezond */
	/* ================= */
	
	/* Theeen */
	switch ($optiondata['label']) {
		case 'Kruidenthee':
			$Theeen[] = $optiondata['value'];
			break;
	}
	/* Bad & Massage Olie */
	switch ($optiondata['label']) {
		case 'Badolie':
			$BadMassage[] = $optiondata['value'];
			break;
		case 'Massageolie':
			$BadMassage[] = $optiondata['value'];
			break;
	}
	/* Fytotherapie */
	switch ($optiondata['label']) {
		case 'Bevordert de Spijsvertering':
			$Fytotherapie[] = $optiondata['value'];
			break;
	}
	/* Homeopathie */
	switch ($optiondata['label']) {
		case '___________________________________________________________________':
			$Homeopathie[] = $optiondata['value'];
			break;
	}
	
	/* ======================== */
	/* Vitaminen & Supplementen */
	/* ======================== */	
	
	/* Specifieke Supplementen */
	switch ($optiondata['label']) {
		case 'Menopauze':
			$SpecifiekeSupp[] = $optiondata['value'];
			break;
	}
	/* Omega 3 Vetzuren */
	switch ($optiondata['label']) {
		case 'Vitaminen':
			$Omega3Vetzuren[] = $optiondata['value'];
			break;
		case 'DHA & EPA':
			$Omega3Vetzuren[] = $optiondata['value'];
			break;
		case 'Omega 3-6-9':
			$Omega3Vetzuren[] = $optiondata['value'];
			break;
	}
	/* Calcium & Mineralen */
	switch ($optiondata['label']) {
		case 'Calcium':
			$CalciumMineralen[] = $optiondata['value'];
			break;
		case 'Vitamine D':
			$CalciumMineralen[] = $optiondata['value'];
			break;
		case 'Magnesium':
			$CalciumMineralen[] = $optiondata['value'];
			break;
		case 'Stress':
			$CalciumMineralen[] = $optiondata['value'];
			break;
	}	
	/* Vitamine A t.e.m. K */
	switch ($optiondata['label']) {
		case 'Calcium':
			$VitamineAK[] = $optiondata['value'];
			break;
		case 'Vitamine D':
			$VitamineAK[] = $optiondata['value'];
			break;
		case 'Vitamine C':
			$VitamineAK[] = $optiondata['value'];
			break;
	}
	/* Studeren, Geheugen & Stress */
	switch ($optiondata['label']) {
		case 'Magnesium':
			$StuderenGeheugen[] = $optiondata['value'];
			break;
		case 'Stress':
			$StuderenGeheugen[] = $optiondata['value'];
			break;
		case 'Geheugen':
			$StuderenGeheugen[] = $optiondata['value'];
			break;
	}	
	/* Multivitaminen */
	switch ($optiondata['label']) {
		case '___________________________________________________________________':
			$Multivitaminen[] = $optiondata['value'];
			break;
	}
	/* Natuurlijk Afweersysteem */
	switch ($optiondata['label']) {
		case '___________________________________________________________________':
			$NatuurlijkAfweer[] = $optiondata['value'];
			break;
	}
	/* Opkikkers & Energie */
	switch ($optiondata['label']) {
		case 'Opkikkers':
			$OpkikkersEnergie[] = $optiondata['value'];
			break;
	}
	/* Probiotica & Spijsvertering */
	switch ($optiondata['label']) {
		case 'Probiotica':
			$ProbioticaSpijs[] = $optiondata['value'];
			break;
	}
	/* Q10, Enzymen & Aminozuren */
	switch ($optiondata['label']) {
		case 'Q10':
			$Q10EnzymeAmino[] = $optiondata['value'];
			break;
	}
	/* Voedingscosmetica */
	switch ($optiondata['label']) {
		case 'Haar':
			$Voedingscosmetica[] = $optiondata['value'];
			break;
		case 'Gezicht':
			$Voedingscosmetica[] = $optiondata['value'];
			break;
	}
}


/*Get collection of all categories*/
$categories=Mage::getModel('catalog/category')->getCollection();

foreach ($categories as $category)
{
	$category_data=Mage::getModel('catalog/category')->load($category['entity_id']);
	
	/* Baby & Zwangerschap */
	if($category_data['name']=='Accessoires'){ $category_data->setAttributecolor($Accessoires)->save(); }
	if($category_data['name']=='Babykwaaltjes'){ $category_data->setAttributecolor($Babykwaaltjes)->save(); }
	if($category_data['name']=='Huidverzorging'){ $category_data->setAttributecolor($Huidverzorging)->save(); }
	if($category_data['name']=='Babymedicatie'){ $category_data->setAttributecolor($Babymedicatie)->save(); }
	if($category_data['name']=='Melk & Voeding'){ $category_data->setAttributecolor($MelkVoeding)->save(); }
	if($category_data['name']=='Vitaminen'){ $category_data->setAttributecolor($Vitaminen)->save(); }
	if($category_data['name']=='Zwangerschap'){ $category_data->setAttributecolor($Zwangerschap)->save(); }	
	
	/* Eerste Hulp & Thuiszorg */
	if($category_data['name']=='EHBO & Verbandmateriaal'){ $category_data->setAttributecolor($EHBOVerband)->save(); }
	if($category_data['name']=='Thuiszorg'){ $category_data->setAttributecolor($Thuiszorg)->save(); }
	if($category_data['name']=='Meten, Wegen & Testen'){ $category_data->setAttributecolor($MetenWegenTesten)->save(); }	
	
	/* Persoonlijke Verzorging */	
 	if($category_data['name']=='Mondverzorging'){ $category_data->setAttributecolor($Mondverzorging)->save(); }
	if($category_data['name']=='Deodorants'){ $category_data->setAttributecolor($Deodorants)->save(); }	
	if($category_data['name']=='Bad & Douche'){ $category_data->setAttributecolor($BadDouche)->save(); }	
	if($category_data['name']=='Zonneproducten'){ $category_data->setAttributecolor($Zonneproducten)->save(); }	
	if($category_data['name']=='Haarverzorging'){ $category_data->setAttributecolor($Haarverzorging)->save(); }	
	if($category_data['name']=='Handen & Voeten'){ $category_data->setAttributecolor($HandenVoeten)->save(); }	
	
	/* Geneesmiddelen */
	
	
	/* Huidverzorging & Make-Up */
	if($category_data['name']=='Dag & Nachtcremes'){ $category_data->setAttributecolor($DagNachtcreme)->save(); }	
	if($category_data['name']=='Olie, Creme & Melk'){ $category_data->setAttributecolor($OlieCremeMelk)->save(); }	
	//
	//
	if($category_data['name']=='Gezichtsreiniging'){ $category_data->setAttributecolor($Gezichtsreiniging)->save(); }	
	if($category_data['name']=='Lipverzorging'){ $category_data->setAttributecolor($Lipverzorging)->save(); }	
	if($category_data['name']=='Make-Up & Parfum'){ $category_data->setAttributecolor($MakeUpParfum)->save(); }	
	if($category_data['name']=='Maskers & Peelings'){ $category_data->setAttributecolor($MaskersPeelings)->save(); }	
	if($category_data['name']=='Oogcontouren'){ $category_data->setAttributecolor($Oogcontouren)->save(); }	
	//
	
	/* Welzijn & Gezondheid */
	if($category_data['name']=='Pijnstilling'){ $category_data->setAttributecolor($Pijnstilling)->save(); }	
	if($category_data['name']=='Ogen & Oren'){ $category_data->setAttributecolor($OgenOren)->save(); }	
	if($category_data['name']=='Maag & Darmen'){ $category_data->setAttributecolor($MaagDarmen)->save(); }	
	if($category_data['name']=='Keel & Ademhaling'){ $category_data->setAttributecolor($KeelAdemhaling)->save(); }	
	if($category_data['name']=='Nachtrust, Ontspanning & Stress'){ $category_data->setAttributecolor($NachtrustOntspanningStress)->save(); }	
	if($category_data['name']=='Vermoeide Benen & Spieren'){ $category_data->setAttributecolor($VermoeideBenenSpieren)->save(); }	
	
	/* Fit & Slank */
	if($category_data['name']=='Afslankende Cremes'){ $category_data->setAttributecolor($AfslankendeCremes)->save(); }	
	if($category_data['name']=='Anti-Cellulitis'){ $category_data->setAttributecolor($AntiCellulitis)->save(); }	
	if($category_data['name']=='Afslankende Thee'){ $category_data->setAttributecolor($AfslankendeThee)->save(); }	
	if($category_data['name']=='Drainage'){ $category_data->setAttributecolor($Drainage)->save(); }	
	if($category_data['name']=='Vetbinders & Vetverbranders'){ $category_data->setAttributecolor($VetbindersVetverbranders)->save(); }	
	if($category_data['name']=='Eetlustremmers'){ $category_data->setAttributecolor($Eetlustremmers)->save(); }
	if($category_data['name']=='Sportvoeding'){ $category_data->setAttributecolor($Sportvoeding)->save(); }
	
	/* Natuurlijk Gezond */	
	if($category_data['name']=='Theeen'){ $category_data->setAttributecolor($Theeen)->save(); }
	if($category_data['name']=='Bad & Massage Olie'){ $category_data->setAttributecolor($BadMassage)->save(); }
	if($category_data['name']=='Fytotherapie'){ $category_data->setAttributecolor($Fytotherapie)->save(); }
	if($category_data['name']=='Homeopathie'){ $category_data->setAttributecolor($Homeopathie)->save(); }
	
	/* Vitaminen & Supplementen */	
	if($category_data['name']=='Specifieke Supplementen'){ $category_data->setAttributecolor($SpecifiekeSupp)->save(); }
	if($category_data['name']=='Omega 3 Vetzuren'){ $category_data->setAttributecolor($Omega3Vetzuren)->save(); }
	if($category_data['name']=='Calcium & Mineralen'){ $category_data->setAttributecolor($CalciumMineralen)->save(); }
	if($category_data['name']=='Vitamine A t.e.m. K'){ $category_data->setAttributecolor($VitamineAK)->save(); }
	if($category_data['name']=='Studeren, Geheugen & Stress'){ $category_data->setAttributecolor($StuderenGeheugen)->save(); }
	if($category_data['name']=='Multivitaminen'){ $category_data->setAttributecolor($Multivitaminen)->save(); }
	if($category_data['name']=='Natuurlijk Afweersysteem'){ $category_data->setAttributecolor($NatuurlijkAfweer)->save(); }
	if($category_data['name']=='Opkikkers & Energie'){ $category_data->setAttributecolor($OpkikkersEnergie)->save(); }
	if($category_data['name']=='Probiotica & Spijsvertering'){ $category_data->setAttributecolor($ProbioticaSpijs)->save(); }
	if($category_data['name']=='Q10, Enzymen & Aminozuren'){ $category_data->setAttributecolor($Q10EnzymeAmino)->save(); }
	if($category_data['name']=='Voedingscosmetica'){ $category_data->setAttributecolor($Voedingscosmetica)->save(); }
}










