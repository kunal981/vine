<?php

require_once 'app' . DIRECTORY_SEPARATOR . 'Mage.php';
Mage::app();

/* ============================================================================================ */
echo '__________________________________________________<br/>';
echo 'GENERAL<br/>';
/** ================================
	Configuration -> General -> ...
================================ **/
Mage::app()->getConfig()->reinit();
$general = array();

$general['country']['fields']['default']['value'] = 'BE';
$general['region']['fields']['display_all']['value'] = '1';
$general['locale']['fields']['timezone']['value'] = 'Europe/Berlin';
$general['locale']['fields']['code']['value'] = 'en_US';
$general['locale']['fields']['firstday']['value'] = '1';
$general['locale']['fields']['weekend']['value'] = ['0','6'];
$general['store_information']['fields']['name']['value'] = 'Heal.be';
$general['store_information']['fields']['phone']['value'] = '123.456.789';
$general['store_information']['fields']['merchant_country']['value'] = 'BE';
$general['store_information']['fields']['merchant_vat_number']['value'] = 'xxx.xxx.xxx';
$general['store_information']['fields']['address']['value'] = 'Vrijheidsplein 4, 9160 Lokeren';

Mage::getModel('adminhtml/config_data')
			->setSection('general')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($general)
			->save();
				
echo 'System -> Configuration -> General<br/>';

/** ================================
	Configuration -> Web -> ...
================================ **/
Mage::app()->getConfig()->reinit();
$web = array();

$web['seo']['fields']['use_rewrites']['value'] = '1';
$web['secure']['fields']['use_in_frontend']['value'] = '1';

Mage::getModel('adminhtml/config_data')
			->setSection('web')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($web)
			->save();
				
echo 'System -> Configuration -> Web<br/>';

/** ================================
	Configuration -> Design -> ...
================================ **/
Mage::app();
Mage::app()->getConfig()->reinit();
$design = array();

$design['package']['fields']['name']['value'] = 'iname';
$design['theme']['fields']['locale']['value'] = 'ame';
$design['theme']['fields']['template']['value'] = 'ame';
$design['theme']['fields']['skin']['value'] = 'ame';
$design['theme']['fields']['layout']['value'] = 'ame';
$design['theme']['fields']['default']['value'] = 'ame';
$design['head']['fields']['default_title']['value'] = 'Heal.be Online Apotheek';
$design['head']['fields']['title_prefix']['value'] = '';
$design['head']['fields']['title_suffix']['value'] = '';
$design['head']['fields']['default_description']['value'] = 'blablabla';
$design['head']['fields']['default_keywords']['value'] = 'Apotheek,Medicatie,Pharmacy';
$design['head']['fields']['default_robots']['value'] = 'INDEX,FOLLOW';
$design['head']['fields']['includes']['value'] = '';
$design['head']['fields']['demonotice']['value'] = '0';
$design['header']['fields']['logo_src']['value'] = 'images/logo.gif';
$design['header']['fields']['logo_alt']['value'] = 'Heal.be Online Apotheek';
$design['header']['fields']['welcome']['value'] = 'Default welcome msg!';
$design['footer']['fields']['copyright']['value'] = '&copy; 2013 Heal.be. All Rights Reserved.';
$design['footer']['fields']['absolute_footer']['value'] = '';
$design['pagination']['fields']['pagination_frame']['value'] = '5';

Mage::getModel('adminhtml/config_data')
			->setSection('design')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($design)
			->save();
				
echo 'System -> Configuration -> Design<br/>';

/** ======================================
	Configuration -> Currency Setup -> ...
====================================== **/
Mage::app()->getConfig()->reinit();
$currency = array();

$currency['options']['fields']['base']['value'] = 'EUR';
$currency['options']['fields']['default']['value'] = 'EUR';
$currency['options']['fields']['allow']['value'] = ['EUR'];

Mage::getModel('adminhtml/config_data')
			->setSection('currency')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($currency)
			->save();
				
echo 'System -> Configuration -> Currency Setup<br/>';

/** ============================================
	Configuration -> Store Email Adresses -> ...
============================================ **/
Mage::app()->getConfig()->reinit();
$trans_email = array();

$trans_email['ident_general']['fields']['name']['value'] = 'Algemeen';
$trans_email['ident_general']['fields']['email']['value'] = 'info@heal.be';
$trans_email['ident_sales']['fields']['name']['value'] = 'Sales';
$trans_email['ident_sales']['fields']['email']['value'] = 'sales@heal.be';
$trans_email['ident_support']['fields']['name']['value'] = 'Klantenservice';
$trans_email['ident_support']['fields']['email']['value'] = 'klantenservice@heal.be';
$trans_email['ident_custom1']['fields']['name']['value'] = 'Support';
$trans_email['ident_custom1']['fields']['email']['value'] = 'support@heal.be';
$trans_email['ident_custom2']['fields']['name']['value'] = 'Technisch';
$trans_email['ident_custom2']['fields']['email']['value'] = 'technisch@heal.be';

Mage::getModel('adminhtml/config_data')
			->setSection('trans_email')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($trans_email)
			->save();
				
echo 'System -> Configuration -> Store Email Adresses Setup<br/>';

/** ============================================
	Configuration -> Contacts -> ...
============================================ **/
Mage::app()->getConfig()->reinit();
$contacts = array();

$contacts['contacts']['fields']['enabled']['value'] = '1';
$contacts['email']['fields']['recipient_email']['value'] = 'lex.mertens@gmail.com';
$contacts['email']['fields']['sender_email_identity']['value'] = 'general';
$contacts['email']['fields']['email_template']['value'] = 'contacts_email_email_template';

Mage::getModel('adminhtml/config_data')
			->setSection('contacts')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($contacts)
			->save();
				
echo 'System -> Configuration -> Contacts Setup<br/>';

/* ============================================================================================ */
echo '__________________________________________________<br/>';
echo 'AME EXTENSIONS<br/>';	
/** ============================================
	Configuration -> Quick View -> ...
============================================ **/
Mage::app()->getConfig()->reinit();
$quickview = array();

$quickview['viewsetting']['fields']['enableview']['value'] = '1';


Mage::getModel('adminhtml/config_data')
			->setSection('quickview')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($quickview)
			->save();
		
echo 'System -> Configuration -> Quick View<br/>';

/** ============================================
	Configuration -> Home Page Slideshow -> ...
============================================ **/
Mage::app()->getConfig()->reinit();
$superslideshow = array();

$superslideshow['general']['fields']['enabled']['value'] = '1';
$superslideshow['general']['fields']['fx']['value'] = 'scrollHorz';
$superslideshow['general']['fields']['easing']['value'] = 'easeInOutCubic';
$superslideshow['general']['fields']['timeout']['value'] = '4000';
$superslideshow['general']['fields']['speed_out']['value'] = '2000';
$superslideshow['general']['fields']['speed_in']['value'] = '2000';
$superslideshow['general']['fields']['sync']['value'] = '1';
$superslideshow['general']['fields']['pause']['value'] = '1';
$superslideshow['general']['fields']['height']['value'] = '60';
$superslideshow['general']['fields']['fit']['value'] = '1';

Mage::getModel('adminhtml/config_data')
			->setSection('superslideshow')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($superslideshow)
			->save();
				
echo 'System -> Configuration -> Home Page Slideshow<br/>';

/* ============================================================================================ */
echo '__________________________________________________<br/>';
echo 'CATALOG<br/>';	
/** ============================================
	Configuration -> Catalog -> ...
============================================ **/
Mage::app()->getConfig()->reinit();
$catalog = array();

$catalog['frontend']['fields']['list_mode']['value'] = 'grid-list';
$catalog['frontend']['fields']['grid_per_page_values']['value'] = '40,80,120';
$catalog['frontend']['fields']['grid_per_page']['value'] = '40';
$catalog['frontend']['fields']['list_per_page_values']['value'] = '5,10,15,20,25';
$catalog['frontend']['fields']['list_per_page']['value'] = '10';
$catalog['frontend']['fields']['list_allow_all']['value'] = '0';
$catalog['frontend']['fields']['default_sort_by']['value'] = 'position';
$catalog['frontend']['fields']['flat_catalog_category']['value'] = '0';
$catalog['frontend']['fields']['flat_catalog_product']['value'] = '0';
$catalog['frontend']['fields']['parse_url_directives']['value'] = '1';
$catalog['sitemap']['fields']['tree_mode']['value'] = '1';
$catalog['sitemap']['fields']['lines_perpage']['value'] = '30';
$catalog['review']['fields']['allow_guest']['value'] = '1';
$catalog['seo']['fields']['site_map']['value'] = '1';
$catalog['seo']['fields']['search_terms']['value'] = '1';
$catalog['seo']['fields']['product_url_suffix']['value'] = '.html';
$catalog['seo']['fields']['category_url_suffix']['value'] = '.html';
$catalog['seo']['fields']['product_use_categories']['value'] = '1';
$catalog['seo']['fields']['save_rewrites_history']['value'] = '1';
$catalog['seo']['fields']['title_separator']['value'] = '-';
$catalog['seo']['fields']['category_canonical_tag']['value'] = '0';
$catalog['seo']['fields']['product_canonical_tag']['value'] = '0';

Mage::getModel('adminhtml/config_data')
			->setSection('catalog')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($catalog)
			->save();
	
echo 'System -> Configuration -> Catalog Setup<br/>';

/* ============================================================================================ */
echo '__________________________________________________<br/>';
echo 'MANADEV<br/>';
/** ============================================
	Configuration -> Layered Navigation -> ...
============================================ **/
Mage::app()->getConfig()->reinit();
$mana_filters = array();

$mana_filters['display']['fields']['category']['value'] = 'standard';
$mana_filters['display']['fields']['price']['value'] = 'slider';
$mana_filters['display']['fields']['decimal']['value'] = 'slider';
$mana_filters['display']['fields']['attribute']['value'] = 'css_checkboxes';
$mana_filters['display']['fields']['sort_method']['value'] = 'byPosition';
$mana_filters['display']['fields']['count']['value'] = '1';
$mana_filters['display']['fields']['disable_no_result_options']['value'] = '1';
$mana_filters['display']['fields']['hide_cms_page_content']['value'] = '1';
$mana_filters['display']['fields']['hide_cms_product_list']['value'] = '0';
$mana_filters['display']['fields']['show_more_item_count']['value'] = '4';
$mana_filters['display']['fields']['show_more_time']['value'] = '200';
$mana_filters['display']['fields']['show_more_method']['value'] = 'scrollbar';

$mana_filters['slider']['fields']['style']['value'] = 'style2';

Mage::getModel('adminhtml/config_data')
			->setSection('mana_filters')
			->setWebsite(Mage::getModel('core/website')->getCode())
			->setGroups($mana_filters)
			->save();
		
echo 'System -> Configuration -> Layered Navigation<br/>';

