(function ($) {
	$(document).ready(function () {
		if (navigator.platform.indexOf("iPad") != -1) {
			$(".top-nav-li").each(function (index, eq) {
				var a = $(eq).find("a:first");
				$(a).attr("rel", $(a).attr("href"));
				$(a).removeAttr("href");
			});
			document.addEventListener("touchstart", touchTopNav, false);
		} else {
			$(".top-nav-con").hover(function () {
					this.canShow = true;
					showTopNavMenu($(this), this);
				},
				function () {
					this.canShow = false;
					closeTopNavMenu(this);
				}
			);
		}
	});
	function showTopNavMenu(obj, objSelected) {
		if (objSelected.canShow) {
			$(obj).find(".top-nav-link").addClass("hovered");
			$(obj).find(".top-shadow-tab").show();
			$(obj).find(".top-nav-menu").show();
			$(obj).find(".top-shadow-tab-ame").show();
			$(obj).find(".top-nav-menu-ame").show();
		}
	}
	function closeTopNavMenu(objSelected) {
		$(objSelected).find(".top-nav-link").removeClass("hovered");
		$(objSelected).find(".top-shadow-tab").hide();
		$(objSelected).find(".top-nav-menu").hide();
		$(objSelected).find(".top-shadow-tab-ame").hide();
		$(objSelected).find(".top-nav-menu-ame").hide();
	}
	function touchTopNav(event) {
		var obj = $(event.touches[0].target).parents(".top-nav-li");
		if (obj.length > 0) {
			if ($(obj).hasClass("touchTopNav")) {
				if ($(event.touches[0].target).attr("rel") != undefined) {
					location.href = $(event.touches[0].target).attr("rel");
					return;
				}
				var temp = $(event.touches[0].target).attr("href");
				if (temp == undefined || temp != "") {
					$(event.touches[0].target).parent().click();
				}
				else {
					closeTopNavMenu($(".touchTopNav"));
					$(".touchTopNav").removeClass("touchTopNav");
				}
				return;
			}
			closeTopNavMenu($(".touchTopNav"));
			$(".touchTopNav").removeClass("touchTopNav");
			$(obj).addClass("touchTopNav");
			obj.canShow = true;
			showTopNavMenu($(obj), obj);
		}
		else {
			closeTopNavMenu($(".touchTopNav"));
			$(".touchTopNav").removeClass("touchTopNav");
		}
	}
		
})(jQuery);