 /*global jQuery */
/*!
 * jQuery Scrollbox
 * (c) 2009-2013 Hunter Wu <hunter.wu@gmail.com>
 * MIT Licensed.
 *
 * http://github.com/wmh/jquery-scrollbox
 */

(function(jQuery) {

jQuery.fn.scrollbox = function(config) {
  //default config
  var defConfig = {
    linear: false,          // Scroll method
    startDelay: 2,          // Start delay (in seconds)
    delay: 3,               // Delay after each scroll event (in seconds)
    step: 5,                // Distance of each single step (in pixels)
    speed: 32,              // Delay after each single step (in milliseconds)
    switchItems: 1,         // Items to switch after each scroll event
    direction: 'vertical',
    distance: 'auto',
    autoPlay: true,
    onMouseOverPause: true,
    paused: false,
    queue: null,
    listElement: 'ul',
    listItemElement:'li'
  };
  config = jQuery.extend(defConfig, config);
  config.scrollOffset = config.direction === 'vertical' ? 'scrollTop' : 'scrollLeft';
  if (config.queue) {
    config.queue = jQuery('#' + config.queue);
  }

  return this.each(function() {
    var homepageslider_container = jQuery(this),
        homepageslider_containerUL,
        scrollingId = null,
        nextScrollId = null,
        paused = false,
        backward,
        forward,
        resetClock,
        scrollForward,
        scrollBackward,
        forwardHover,
        pauseHover;

    if (config.onMouseOverPause) {
      homepageslider_container.bind('mouseover', function() { paused = true; });
      homepageslider_container.bind('mouseout', function() { paused = false; });
    }
    homepageslider_containerUL = homepageslider_container.children(config.listElement + ':first-child');

    scrollForward = function() {
      if (paused) {
        return;
      }
      var curLi,
          i,
          newScrollOffset,
          scrollDistance,
          theStep;

      curLi = homepageslider_containerUL.children(config.listItemElement + ':first-child');

      scrollDistance = config.distance !== 'auto' ? config.distance :
        config.direction === 'vertical' ? curLi.outerHeight(true) : curLi.outerWidth(true);

      // offset
      if (!config.linear) {
        theStep = Math.max(3, parseInt((scrollDistance - homepageslider_container[0][config.scrollOffset]) * 0.3, 10));
        newScrollOffset = Math.min(homepageslider_container[0][config.scrollOffset] + theStep, scrollDistance);
      } else {
        newScrollOffset = Math.min(homepageslider_container[0][config.scrollOffset] + config.step, scrollDistance);
      }
      homepageslider_container[0][config.scrollOffset] = newScrollOffset;

      if (newScrollOffset >= scrollDistance) {
        for (i = 0; i < config.switchItems; i++) {
          if (config.queue && config.queue.find(config.listItemElement).length > 0) {
            homepageslider_containerUL.append(config.queue.find(config.listItemElement)[0]);
            homepageslider_containerUL.children(config.listItemElement + ':first-child').remove();
          } else {
            homepageslider_containerUL.append(homepageslider_containerUL.children(config.listItemElement + ':first-child'));
          }
        }
        homepageslider_container[0][config.scrollOffset] = 0;
        clearInterval(scrollingId);
        if (config.autoPlay) {
          nextScrollId = setTimeout(forward, config.delay * 1000);
        }
      }
    };

    // Backward
    // 1. If forwarding, then reverse
    // 2. If stoping, then backward once
    scrollBackward = function() {
      if (paused) {
        return;
      }
      var curLi,
          i,
          liLen,
          newScrollOffset,
          scrollDistance,
          theStep;

      // init
      if (homepageslider_container[0][config.scrollOffset] === 0) {
        liLen = homepageslider_containerUL.children(config.listItemElement).length;
        for (i = 0; i < config.switchItems; i++) {
          homepageslider_containerUL.children(config.listItemElement + ':last-child').insertBefore(homepageslider_containerUL.children(config.listItemElement+':first-child'));
        }

        curLi = homepageslider_containerUL.children(config.listItemElement + ':first-child');
        scrollDistance = config.distance !== 'auto' ?
            config.distance :
            config.direction === 'vertical' ? curLi.height() : curLi.width();
        homepageslider_container[0][config.scrollOffset] = scrollDistance;
      }

      // new offset
      if (!config.linear) {
        theStep = Math.max(3, parseInt(homepageslider_container[0][config.scrollOffset] * 0.3, 10));
        newScrollOffset = Math.max(homepageslider_container[0][config.scrollOffset] - theStep, 0);
      } else {
        newScrollOffset = Math.max(homepageslider_container[0][config.scrollOffset] - config.step, 0);
      }
      homepageslider_container[0][config.scrollOffset] = newScrollOffset;

      if (newScrollOffset === 0) {
        clearInterval(scrollingId);
        if (config.autoPlay) {
          nextScrollId = setTimeout(forward, config.delay * 1000);
        }
      }
    };

    forward = function() {
      clearInterval(scrollingId);
      scrollingId = setInterval(scrollForward, config.speed);
    };

    // Implements mouseover function.
    forwardHover = function() {
        config.autoPlay = true;
        paused = false;
        clearInterval(scrollingId);
        scrollingId = setInterval(scrollForward, config.speed);
    };
    pauseHover = function() {
        paused = true;
    };

    backward = function() {
      clearInterval(scrollingId);
      scrollingId = setInterval(scrollBackward, config.speed);
    };

    resetClock = function(delay) {
      config.delay = delay || config.delay;
      clearTimeout(nextScrollId);
      if (config.autoPlay) {
        nextScrollId = setTimeout(forward, config.delay * 1000);
      }
    };

    if (config.autoPlay) {
      nextScrollId = setTimeout(forward, config.startDelay * 1000);
    }

    // bind events for homepageslider_container
    homepageslider_container.bind('resetClock', function(delay) { resetClock(delay); });
    homepageslider_container.bind('forward', function() { clearTimeout(nextScrollId); forward(); });
    homepageslider_container.bind('pauseHover', function() { pauseHover(); });
    homepageslider_container.bind('forwardHover', function() { forwardHover(); });
    homepageslider_container.bind('backward', function() { clearTimeout(nextScrollId); backward(); });
    homepageslider_container.bind('speedUp', function(speed) {
      if (typeof speed === 'undefined') {
        speed = Math.max(1, parseInt(config.speed / 2, 10));
      }
      config.speed = speed;
    });
    
    homepageslider_container.bind('speedDown', function(speed) {
      if (typeof speed === 'undefined') {
        speed = config.speed * 2;
      }
      config.speed = speed;
    });

    homepageslider_container.bind('updateConfig', function (event,options) {
        config = jQuery.extend(config, options);
    });

  });
};

}(jQuery));
