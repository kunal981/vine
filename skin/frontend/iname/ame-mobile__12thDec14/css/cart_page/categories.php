<?php
require_once ("../app/Mage.php");
Mage::app();
umask(0);
ob_start();

function get_categories()
{
    try
    {
        $parent_category=Mage::getModel('catalog/category')->load(2);
        $childs=$parent_category->getChildren();
        $childs=explode(",",$childs);
        $i=0;
        foreach($childs as $child)
        {
            $p_cat=Mage::getModel('catalog/category')->load($child);
            $name=$p_cat->getName();
            $id=$p_cat->getId();
            if($p_cat->getImageUrl()!=FALSE)
            {
                $image=$p_cat->getImageUrl();
            }
            else
            {
                $image="";
            }
            $category[$i]=array("name"=>$name,"id"=>$id,"image"=>$image);
            $i++;
        }
        $send_data["success"]=true;
        $send_data["categories"]=$category;
        echo json_encode($send_data);
    }
    catch( Exception $e )
    {
        $send_data["success"]=false;
        $send_data["message"]="No categories are available";
        echo json_encode($send_data);
        return false;
    }
}
get_categories();

?>