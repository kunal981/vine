
/* Style of the small image link */
.MagicZoomPlus, .MagicZoomPlus:hover {
    display: inline-block;
    cursor: url(graphics/zoomin.cur), pointer;
    outline: 0 !important;
    font-size: 0 !important;
    line-height: 100% !important;
    direction: ltr;
    max-width: 100%;
}

/* Style of the small image */
.MagicZoomPlus img {
    border: 0 !important;
    margin: 0 !important;
    outline: 0 !important;
    padding: 0 !important;
    height: auto;
}

.MagicZoomPlus > img {
    width: 100%;
}

.MagicZoomPlus.magic-for-ie8 > img {
    max-width: none !important;
}

.MagicZoomPlus.magic-for-ie7 > img, .MagicZoomPlus.magic-for-ie5 img {
    width: auto;
}

td > .MagicZoomPlus > img, td > .MagicZoomPlus.magic-for-ie8 > img {
    max-width: none;
    width: auto;
}


/* Style of the span inside the small image link */
.MagicZoomPlus span {
    display: none !important;
}

/* Style of hint box */
.MagicZoomPlusHint {
    background: url(graphics/hint.gif) no-repeat 2px 50%;
    color: #444;
    font-family: sans-serif;
    font-size: 8pt;
    line-height: 24px;
    margin: 0;
    min-height: 24px;
    padding: 2px 2px 2px 20px !important;
    text-align: left;
    text-decoration: none;
}

.MagicZoomPlusLoading {
    background: #fff url(graphics/loader.gif) no-repeat 2px 50%;
    border: 1px solid #ccc;
    color: #444;
    font-family: sans-serif;
    font-size: 8pt;
    line-height: 1.5em;
    margin: 0;
    padding: 4px 4px 4px 24px !important;
    text-align: left;
    text-decoration: none;
}


/* Style to hide external title or caption on a page. Only needed for #id method */
.MagicZoomPlus-ExternalText {
    display: none;
}


/* Style of the zoomed image */
.MagicZoomBigImageCont {
    background: #ffffff;
    border: 1px solid #999;
    -webkit-transition: none !important;
    -moz-transition: none !important;
    transition: none !important;
}
.MagicZoomBigImageCont.inner-zoom {
    border: 0;
}

.MagicZoomBigImageCont img {
    max-width: none !important;
    max-height: none !important;
    height: auto !important;
    width: auto !important;
}

/* Style of text on the zoomed image */
.MagicZoomHeader {
    background: #666;
    color: #fff;
    font-size: 10pt !important;
    line-height: normal !important;
    text-align: center !important;
}

/* Style of square magnify area under the cursor */
.MagicZoomPup {
    background: #dadad1;
    border: 3px solid #aaa;
    cursor:url(graphics/zoomin.cur),pointer;
}

/* Style of loading message and icon shown during load */
.MagicZoomLoading {
    background: #fff url(graphics/loader.gif) no-repeat 2px 50%;
    border: 1px solid #ccc;
    color: #444;
    font-family: sans-serif;
    font-size: 8pt;
    line-height: 1.5em;
    margin: 0;
    padding: 4px 4px 4px 24px !important;
    text-align: left;
    text-decoration: none;
}

/* Style of hotspots */
.MagicHotspots {
display: none;
visibility: hidden;
}
.MagicHotspots a {
border: 1px solid #ccc;
display: block !important;
position: absolute !important;
}
.MagicHotspots a:hover {
border: 1px solid red;
}

/* Style of the small image when the large image is expanded */
.MagicThumb-expanded-thumbnail {
    cursor: default;
}

/* Style of the expanded image */
.MagicThumb-expanded {
    background: #ffffff;
    border: 1px solid #ccc;
    cursor: url(graphics/zoomout.cur), pointer;
    outline: 0;
    padding: 0;
    -webkit-transition: none !important;
    -moz-transition: none !important;
    transition: none !important;
}

.MagicThumb-expanded img {
    background: #ffffff;
}

/* Style of the caption for the expanded image */
.MagicThumb-caption {
    background: #ccc;
    border: 0 !important;
    color: #333;
    font: normal 10pt Verdana, Helvetica;
    min-height: 18px !important;
    padding: 8px;
    outline: 0 !important;
    text-align: left;
}

/* Style of the close/next/previous buttons */
.MagicThumb-buttons {
    background: transparent url(graphics/buttons1.png) no-repeat 0 0;
    display: block;
    height: 24px;
}

.MagicThumb-buttons a {
    height: 24px;
    margin: 0px 1px !important;
    overflow: hidden;
    width: 24px;
    -webkit-transition: none !important;
    -moz-transition: none !important;
    transition: none !important;
}

.MagicThumb-expanded > div > div > div > img {
    max-height: none !important;
    max-width: none !important;
    height: auto !important;
    width: auto !important;
}

.MagicThumb-background {
    -webkit-transition: none !important;
    -moz-transition: none !important;
    transition: none !important;
}

.magic-temporary-img img {
    max-height: none !important;
    max-width: none !important;
}

/* Style of shadow effect behind zoomed image */
.MagicBoxShadow {
-moz-box-shadow: 3px 3px 4px #888888;
-webkit-box-shadow: 3px 3px 4px #888888;
box-shadow: 3px 3px 4px #888888;
border-collapse: separate;
/* For IE 5.5 - 7 */
filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#888888') !important;
/* For IE 8 */
-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#888888')" !important;
}

/* Style of glow effect behind zoomed image */
.MagicBoxGlow {
-moz-box-shadow: 0px 0px 4px 4px #888888;
-webkit-box-shadow: 0px 0px 4px 4px #888888;
box-shadow: 0px 0px 4px 4px #888888;
border-collapse: separate;
/* For IE 5.5 - 7 */
filter: progid:DXImageTransform.Microsoft.Glow(Strength=4, Color='#888888') !important;
/* For IE 8 */
-ms-filter: "progid:DXImageTransform.Microsoft.Glow(Strength=4, Color='#888888')" !important;
}

/* Contaner with main image and selectors container */
div.AmeContainer {
    text-align: center;
    max-width: none !important;    
    width: 440px;
}

/* Main image styles */
div.AmeContainer a {
    margin: 0 auto;
}

/* Container with selectors*/
div.AmeSelectorsContainer {
    clear: both;
    text-align: left;
    margin-top: 22px !important;
}

/* Selectors styles */
div.AmeSelectorsContainer a {
    display: inline-block;
    margin: 4px 5px;
    border: 1px solid #ccc;
    padding: 5px 12px;
}

div.AmeMessage {
    text-align: center;
}

/* selectors-left */
div.AmeContainer.selectorsLeft .AmeMainContainer {
    float: left;
}
div.AmeContainer.selectorsLeft .AmeSelectorsContainer {
    float: left;
}

/* selectors-right */
div.AmeContainer.selectorsRight .AmeMainContainer {
    float: right;
}
div.AmeContainer.selectorsRight .AmeSelectorsContainer {
    float: right;
}

.MagicScrollItem a {
    /* NOTE: magicscroll makes the image smaller if there is a margin */
    margin: 0 !important;
}
div.AmeSelectorsContainer {
    clear: none;
}
.product-view .product-img-box .more-views ul {
    margin-left: 0px;
}
.products-list .AmeContainer {
    /* for List View */
    float: left;
}
.hidden-selector {
    display: none !important;
}
/* issue #36402 */
.MagicZoom, .MagicZoomPlus {
    opacity: 1 !important;
    filter: none !important;
}
.ignore-magento-css {
    width: auto !important;
    height: auto !important;
    margin: 0 !important;
}
.product-img-box {
    text-align: center;
}

.MagicThumb-expanded  span{display: none !important;}
.MagicZoomBigImageCont.inner-zoom div{visibility: hidden;}

.MagicZoomBigImageCont.inner-zoom div img{visibility: visible;}

.MagicZoomBigImageCont div img{visibility: visible}
.MagicZoomBigImageCont div{visibility: hidden}

.MagicThumb-expanded #undefined{visibility: visible}
.MagicThumb-expanded #undefined img{visibility: visible;}
.MagicThumb-expanded #undefined div.MagicThumb-buttons{visibility: visible;}
.MagicThumb-expanded #undefined div{visibility: hidden !important;}
.MagicThumb-expanded #undefined .MagicZoomBigImageCont{visibility: visible} 
.MagicThumb-expanded #undefined .inner-zoom{visibility: visible} 

.MagicThumb-expanded div.MagicThumb-buttons{visibility: visible}
.MagicThumb-expanded div img{visibility: visible}
.MagicThumb-expanded div.MagicZoomBigImageCont {visibility: visible}
.MagicThumb-expanded div.inner-zoom{visibility: visible}
.MagicThumb-expanded div{visibility: hidden !important}
.MagicThumb-expanded div div.MagicThumb-caption{visibility: visible !important;}
