jQuery(document).ready(function(){
	var pagebody = jQuery("#pagebody");
	var themenu  = jQuery("#navmenu");
	var topbar   = jQuery("#toolbarnav");
	var content  = jQuery("#content");
	var viewport = {
    	width  : jQuery(window).width(),
    	height : jQuery(window).height()
	};
	// retrieve variables as 
	// viewport.width / viewport.height
	
	function openme() { 
		jQuery(function () {
		    topbar.animate({
		       left: "250px"
		    }, { duration: 300, queue: false });
		    pagebody.animate({
		       left: "250px"
		    }, { duration: 300, queue: false });
		});
	}
	
	function closeme() {
		var closeme = jQuery(function() {
	    	topbar.animate({
	            left: "0px"
	    	}, { duration: 180, queue: false });
	    	pagebody.animate({
	            left: "0px"
	    	}, { duration: 180, queue: false });
		});
	}

	// checking whether to open or close nav menu
	jQuery("#menu-btn").live("click", function(e){
		e.preventDefault();
		var leftval = pagebody.css('left');
		
		if(leftval == "0px") {
			openme();
		}
		else { 
			closeme(); 
		}
	});
});