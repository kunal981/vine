jQuery(document).ready( function() {
    jQuery("#link1,#closeLink1,#blanket1").click( function () { popup('popUpDiv1')});
});

function toggle(div_id) {
	var el = document.getElementById(div_id);
	if ( el.style.display == 'none' ) {	el.style.display = 'block';}
	else {el.style.display = 'none';}
}
function blanket1_size(popUpDiv1Var) {
	if (typeof window.innerWidth != 'undefined') {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket1_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket1_height = document.body.parentNode.clientHeight;
		} else {
			blanket1_height = document.body.parentNode.scrollHeight;
		}
	}
	var blanket1 = document.getElementById('blanket1');
	blanket1.style.height = blanket1_height + 'px';
	var popUpDiv1 = document.getElementById(popUpDiv1Var);
	popUpDiv1_height=blanket1_height/2-200;//200 is half popup's height
	popUpDiv1.style.top = popUpDiv1_height + 'px';
}
function window_pos(popUpDiv1Var) {
	if (typeof window.innerWidth != 'undefined') {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv1 = document.getElementById(popUpDiv1Var);
	window_width=window_width/2-200;//200 is half popup's width
	popUpDiv1.style.left = window_width + 'px';
}
function popup(windowname) {
	blanket1_size(windowname);
	window_pos(windowname);
	toggle('blanket1');
	toggle(windowname);		
}